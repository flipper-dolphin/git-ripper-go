package main

import (
	"flag"
	"os"

	"github.com/go-git/go-git/v5"
)

func main() {

	email_map := start()

	// Output
	for email := range email_map {
		println(email)
	}
}

func start() map[string]struct{} {

	repoPtr := flag.String("r", "", "A repo to scrape")
	usrPtr := flag.String("u", "", "A user to scrape")
	flag.Parse()
	// emails := make(map[string]struct{})

	if *repoPtr != "" {
		println("Scraping Repo:", *repoPtr)
		println("Cloning, please be patient for bigger repos")
		emails := scrape(*repoPtr)
		println(len(emails))
		return emails
	} else if *usrPtr != "" {
		println("User: ", *usrPtr)
		// TODO: Add logic here for scraping a user
		return nil
	} else {
		println("No user or repo given")
		os.Exit(1)
	}
	os.Exit(1)
	return nil
}

func scrape(url string) map[string]struct{} {
	// Clone the given repository to memory
	// r, err := git.Clone(memory.NewStorage(), nil, &git.CloneOptions{
	// 	URL:      url,
	// 	Progress: os.Stdout,
	// })
	os.RemoveAll("./.tmp/")
	// Figure out how to make this clone to memory like above but also bare clone
	r, err := git.PlainClone("./.tmp/git-ripper", true, &git.CloneOptions{
		URL: url,
		// Progress: os.Stdout,
	})
	if err != nil {
		panic("git repo failed to clone - check your URL")
	}

	emails := make(map[string]struct{})

	commits, _ := r.CommitObjects()
	for i, _ := commits.Next(); err == nil; i, err = commits.Next() {
		emails[i.Author.Email] = struct{}{}
	}

	os.RemoveAll("./.tmp/")
	return emails

}
